#!/usr/bin/bash
#

# EXERCISE 2: Bash Script - Install Java
# 
# Write a bash script using Vim editor that installs the latest java version and
# checks whether java was installed successfully by executing a java --version | head -1 | awk {'print '} command.
# 
# After installation command, it checks 3 conditions:
# 
#     1. whether java is installed at all
#     2. whether an older Java version is installed (java version lower than 11)
#     3. whether a java version of 11 or higher was installed
# 
# It prints relevant informative messages for all 3 conditions. Installation was successful 
# if the 3rd condition is met and you have Java version 11 or higher available.
#
# install java, if it is not installed yet
if ! java --version > /dev/null 2>&1 ; then
   echo "java not installed, installing in 4 seconds" && sleep 4s
   echo "installing java now. I'll let you know once I am finished"
   sudo apt install -y default-jdk > /tmp/java_install.out 2>&1 &
   while ! java --version > /dev/null 2>&1 ; do
	   sleep 10
	   echo "  ... still installing, sit tight"
   done
   echo "installation of java $(java --version | head -1 | awk {'print $2'}) is finished."
   echo "Check '/tmp/java_install.out' for the details" ; sleep 3
fi

java_version="$(java --version | head -1 | awk {'print $2'})"

#  2. wether an older Java version is installed (java version lower than 11)
if [ $(java --version | head -1 | awk {'print $2'} | cut -d '.' -f 1) -lt 11 ] ; then
	echo "the installed java version ${java_version} is SMALLER then 11"
else
	echo "the installed java version ${java_version} is LARGER then 11"
fi

# https://linuxiac.com/how-to-install-java-on-debian-12-bookworm/
# https://itslinuxfoss.com/install-java-debian-12-linux/

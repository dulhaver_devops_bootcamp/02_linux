# ##############################################################################
#!/usr/bin/bash ################################################################
# ##############################################################################
#
# EXERCISE 9: node app with service user
# extend script 08, so it 
#  - adds a service user 'bootcamp_app01' 
#  - runs the application with this user
#

DOWNLOAD_DIR='https://node-envvars-artifact.s3.eu-west-2.amazonaws.com'
MY_APP='bootcamp-node-envvars-project'
APP_VERSION='1.0.0'
LOG_DIR='/tmp/log/'
APP_USER='bootcamp_appuser01'
# set -x

# check for nodejs/npm and create a return code for each
# njs_rc=$(node --version >/dev/null 2>&1)
# njs_rc=$?
# npm_rc=$(npm --version >/dev/null 2>&1)
# npm_rc=$?
# 
# echo njs_rc: "${njs_rc}" \| npm_rc: "${npm_rc}" ; echo
# sleep 3

# ### install nodejs and npm if they are not installed
# if [ ! ${njs_rc} -eq 0 ] || [ ! ${npm_rc} -eq 0 ] ; then
if ! node --version > /dev/null 2>&1 ; then 	# || npm --version > /dev/null 2>&1 ; then
      echo 'nodejds is not installed, getting it for you now' ; sleep 3 ; echo
      sudo apt update 
      sudo apt install -y ca-certificates curl gnupg
      if [ ! -d /etc/apt/keyrings ] ; then 
	      sudo mkdir -p /etc/apt/keyrings
      fi
      curl -fsSL https://deb.nodesource.com/gpgkey/nodesource-repo.gpg.key | \
	      sudo gpg --dearmor -o /etc/apt/keyrings/nodesource.gpg
      NODE_MAJOR=20
      echo "deb [signed-by=/etc/apt/keyrings/nodesource.gpg] https://deb.nodesource.com/node_${NODE_MAJOR}.x nodistro main" | \
	      sudo tee /etc/apt/sources.list.d/nodesource.list
      sudo apt update && sudo apt install -u nodejs
      # print out which versions were installed
      echo installed node $(node --version) and npm $(npm --version) from the nodejs repo ; sleep 5 ; echo
else
      echo ; echo nodejs $(node --version) \& npm $(npm --version) are already installed ; sleep 5 ; echo
fi

# Download an artifact file from the URL: https://node-envvars-artifact.s3.eu-west-2.amazonaws.com/bootcamp-node-envvars-project-1.0.0.tgz. Hint: use curl or wget
if [ ! -d ex06_"${MY_APP}" ] ; then
	mkdir ex06_"${MY_APP}"
	cd    ex06_"${MY_APP}"
	echo I am now in $(pwd), proceeding with download in 3 seconds ; sleep 3
	curl "${DOWNLOAD_DIR}"/"${MY_APP}"-"${APP_VERSION}".tgz --output ./"${MY_APP}"-"${APP_VERSION}".tgz
	# Unzip the downloaded file
	tar -xvzf "${MY_APP}"-"${APP_VERSION}".tgz
else
	cd    ex06_"${MY_APP}"
fi


# Change into the unzipped package directory
cd package

# Set the following needed environment variables: APP_ENV=dev, DB_USER=myuser, DB_PWD=mysecret
sudo -su $APP_USER 
export APP_ENV=dev
export DB_USER=myuser
export DB_PWD=mysecret

# get the path for $LOG_DIR from the user & make sure is exists
read -p "gimme logdir (full path): " input
export LOG_DIR=${input}
# echo ; echo log dir is: ${LOG_DIR} ; sleep 3


if [ ! -d ${LOG_DIR} ] ; then
	mkdir ${LOG_DIR}
	echo creating ${LOG_DIR} ; sleep 3
else
	echo ${LOG_DIR} exists already ; sleep 3
fi

echo these are the set variables: ${APP_ENV} --- ${DB_USER} --- ${DB_PWD} --- ${LOG_DIR} ; sleep 5

#  - adds a service user 'bootcamp_app01' 
echo  "adding a service user 'bootcamp_app01' now"
sudo useradd "${APP_USER}"

#  - run the application with this user
echo "running the application with this 'bootcamp_app01' user"

echo 

# Run the NodeJS application by executing the following commands:  npm install and node server.js 
npm install
# nohup node server.js & >> "${LOG_DIR}"/node_$(date +%F).log 2>&1
# sudo runuser -u "${APP_USER}" -- node server.js & # >> "${LOG_DIR}"/node_$(date +%F).log 2>&1
node server.js & # >> "${LOG_DIR}"/node_$(date +%F).log 2>&1

# check that the application has successfully started
ps -xU ${USER} | grep node > /dev/null 2>&1


# print out the application's running process and the port where it's listening. 
export NODE_PROCESS=$(ps -x | grep server.js | head -1 | awk {'print $1'})
# export NODE_PORT=$(sudo netstat -tulpn | grep "${NODE_PROCESS}" | awk {'print $4'} | sed "s/://g")
export NODE_PORT=$(ss -tulpn | grep :3000 | awk {'print $5'} | sed "s/*://")
echo ; echo our program runs with process-id: "${NODE_PROCESS}" under port: "${NODE_PORT}"


#!/usr/bin/bash

# EXERCISE 9: node app with service user
# extend script 08, so it 
#  - adds a service user 'bootcamp_app01' 
#  - runs the application with this user
#

DOWNLOAD_DIR='https://node-envvars-artifact.s3.eu-west-2.amazonaws.com'
MY_APP='bootcamp-node-envvars-project'
APP_DIR="/opt/ex06/${MY_APP}"
APP_VERSION='1.0.0'
LOG_DIR='/tmp/log/'
APP_USER='bootcamp_appuser01'

set -euo pipefail

# check for nodejs/npm and create a return code for each
# njs_rc=$(node --version >/dev/null 2>&1)
# njs_rc=$?
# npm_rc=$(npm --version >/dev/null 2>&1)
# npm_rc=$?
# 
# echo njs_rc: "${njs_rc}" \| npm_rc: "${npm_rc}" ; echo
# sleep 3

# ### install nodejs and npm if they are not installed
# if [ ! ${njs_rc} -eq 0 ] || [ ! ${npm_rc} -eq 0 ] ; then
if ! node --version > /dev/null 2>&1 ; then 	# || npm --version > /dev/null 2>&1 ; then
	echo 'nodejds is not installed, getting it for you now' ; sleep 3 ; echo
	sudo apt update 
	sudo apt install -y ca-certificates curl gnupg
	if [ ! -d /etc/apt/keyrings ] ; then 
		sudo mkdir -p /etc/apt/keyrings
	fi
	curl -fsSL https://deb.nodesource.com/gpgkey/nodesource-repo.gpg.key | \
		sudo gpg --dearmor -o /etc/apt/keyrings/nodesource.gpg
	NODE_MAJOR=20
	echo "deb [signed-by=/etc/apt/keyrings/nodesource.gpg] https://deb.nodesource.com/node_${NODE_MAJOR}.x nodistro main" | \
		sudo tee /etc/apt/sources.list.d/nodesource.list
	sudo apt update && sudo apt install -u nodejs
	# print out which versions were installed
	echo installed node "$(node --version)" \& npm "$(npm --version)" from the nodejs repo ; sleep 3 ; echo
else
	echo ; echo nodejs "$(node --version)" \& npm "$(npm --version)" are already installed ; sleep 3 ; echo
fi


# read user input for log directory
echo -n "Set log directory location for the application (absolute path): "
read LOG_DIRECTORY
if [ -d $LOG_DIRECTORY ] ; then
  echo "$LOG_DIRECTORY already exists"
fi

# create new user to run the application and make owner of log dir
useradd $APP_USER -m
chown $APP_USER -R $LOG_DIRECTORY

# executing the following commands as new user using 'runuser' command

# fetch NodeJS project archive from s3 bucket
runuser -l $APP_USER -c "wget https://node-envvars-artifact.s3.eu-west-2.amazonaws.com/bootcamp-node-envvars-project-1.0.0.tgz"

# extract the project archive to ./package folder
runuser -l $APP_USER -c "tar zxvf ./bootcamp-node-envvars-project-1.0.0.tgz"

# start the nodejs application in the background, with all needed env vars with new user myapp
runuser -l $APP_USER -c "
    export APP_ENV=dev && 
    export DB_PWD=mysecret && 
    export DB_USER=myuser && 
    export LOG_DIR=$LOG_DIRECTORY && 
    cd package && 
    npm install && 
    node server.js &"

# display that nodejs process is running
ps aux | grep node | grep -v grep

# display that nodejs is running on port 3000
netstat -ltnp | grep :3000

exit 0


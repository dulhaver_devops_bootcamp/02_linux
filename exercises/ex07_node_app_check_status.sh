# ##############################################################################
#!/usr/bin/bash ################################################################
# ##############################################################################
#
# EXERCISE 7: Bash Script - Node App Check Status
#
# Extend script 6 to check after running the application that the 
#  - application has successfully started and
#  - prints out the application's running process and the port where it's listening. 


DOWNLOAD_DIR='https://node-envvars-artifact.s3.eu-west-2.amazonaws.com'
MY_APP='bootcamp-node-envvars-project'
APP_VERSION='1.0.0'

# set -x

# check for nodejs/npm and create a return code for each
njs_rc=$(node --version >/dev/null 2>&1)
njs_rc=$?
npm_rc=$(npm --version >/dev/null 2>&1)
npm_rc=$?

# echo njs_rc: "${njs_rc}" \| npm_rc: "${npm_rc}" ; echo
# sleep 3

# ### install nodejs and npm if they are not installed
if [ ! ${njs_rc} -eq 0 ] || [ ! ${npm_rc} -eq 0 ] ; then
      echo 'nodejds is not installed, getting it for you now' ; echo
      sudo curl -fsSL https://deb.nodesource.com/setup_20.x | sudo -E bash - && sudo apt install nodejs -y
      #    curl -fsSL https://deb.nodesource.com/setup_20.x | sudo -E bash - && sudo apt install nodejs -y
      # print out which versions were installed
      echo installed node $(node --version) and npm $(npm --version) from the nodejs repo ; echo
else
      echo ; echo nodejs $(node --version) \& npm $(npm --version) are already installed ; echo
fi


# Download an artifact file from the URL: https://node-envvars-artifact.s3.eu-west-2.amazonaws.com/bootcamp-node-envvars-project-1.0.0.tgz. Hint: use curl or wget


if [ ! -d ex06_"${MY_APP}" ] ; then
	mkdir ex06_"${MY_APP}"
	cd    ex06_"${MY_APP}"
	echo I am now in $(pwd), proceeding with download in 3 seconds ; sleep 3
	curl "${DOWNLOAD_DIR}"/"${MY_APP}"-"${APP_VERSION}".tgz --output ./"${MY_APP}"-"${APP_VERSION}".tgz
	# Unzip the downloaded file
	tar -xvzf "${MY_APP}"-"${APP_VERSION}".tgz
else
	cd    ex06_"${MY_APP}"
fi


# Change into the unzipped package directory
cd package	# "${MY_APP}"-"${APP_VERSION}"
echo "I am in: $(pwd)" ; echo

# Set the following needed environment variables: APP_ENV=dev, DB_USER=myuser, DB_PWD=mysecret
export APP_ENV=dev ; export DB_USER=myuser ; export DB_PWD=mysecret ; export LOG_DIR='/tmp/nodejs_log/'
echo "these are the set variables: ${APP_ENV}" --- "${DB_USER}" --- "${DB_PWD}" --- ${LOG_DIR}
# make sure ${LOG_DIR} exits
if [ ! -d "${LOG_DIR}" ] ; then
	mkdir "${LOG_DIR}"
	echo creating "${LOG_DIR}"
fi

echo 
echo running the app in 3 seconds ; sleep 3

# Run the NodeJS application by executing the following commands:  npm install and node server.js 
npm install
nohup node server.js & >> "${LOG_DIR}"/node_$(date +%F).log 2>&1

# check that the application has successfully started
ps -xU $USER | grep node > /dev/null 2>&1


# print out the application's running process and the port where it's listening. 
export NODE_PROCESS=$(sudo ps -x | grep server.js | head -1 | awk {'print $1'})
export NODE_PORT=$(sudo netstat -tulpn | grep "${NODE_PROCESS}" | awk {'print $4'} | sed "s/://g")

echo ; echo our program runs with process-id: "${NODE_PROCESS}" under port: "${NODE_PORT}"


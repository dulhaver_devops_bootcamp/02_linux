#!/usr/bin/bash
#

set -euo pipefail

echo 		"this script was run by $(whoami) @ $HOME"
echo 		$HOME

echo ; echo 	"this script is running this command as $(runuser -l bootcamp_appuser01 -c whoami)" 

echo		this is my home dir: $(runuser -l bootcamp_appuser01 -c echo "$HOME")

# ##############################################################################
#!/usr/bin/bash ################################################################
# ##############################################################################

# EXERCISE 5: Bash Script - Number of User Processes Sorted
# 
# Extend the previous script to ask additionally for user input about how many processes to print.
# Hint: use head program to limit the number of outputs.

sorter='0'

read -p "gimme 'memory', or 'cpu' consumption ? " sorter
read -p "how many processes do you want to see? " counter

# make sure you get workable input (memory, cpu and nothing else
while [ "${sorter}" != 'memory' ] && [ "${sorter}" != 'cpu' ] ; do
	echo
	read -p "sorry, ${sorter} is no valid input. Please choose 'memory'. or 'cpu': " sorter
	echo
done


echo ; echo these are top ${counter} user processes sorted by "${sorter}" load ; echo


# decide which road to take and execute the respective listing
if [ "${sorter}" = 'memory' ] ; then
	ps -xu -U ${USER} --sort=-%mem | head -n $(( ${counter} + 1 ))
	# echo =======================
	# ps xu --sort=-%mem
else
	ps -xu -U ${USER} --sort=-%cpu | head -n $(( ${counter} + 1 ))
fi

echo

exit 0

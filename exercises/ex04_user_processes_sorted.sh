# ##############################################################################
#!/usr/bin/bash ################################################################
# ##############################################################################

# EXERCISE 4: Bash Script - User Processes Sorted
# 
# Extend the previous script to ask for a user input for sorting the processes output 
# either by memory or CPU consumption, and print the sorted list.

sorter='0'

read -p "du you want to sort processes by 'memory', or 'cpu' consumption ? " sorter

while [ "${sorter}" != 'memory' ] && [ "${sorter}" != 'cpu' ] ; do
	echo
	read -p "sorry, ${sorter} is no valid input. Please choose 'memory'. or 'cpu': " sorter
	echo
done

echo ; echo these are the user processes sorted by "${sorter}" load ; echo

if [ "${sorter}" = 'memory' ] ; then
	ps -xu -U ${USER} --sort=-%mem
	# echo =======================
	# ps xu --sort=-%mem
else
	ps -xu -U ${USER} --sort=-%cpu
fi

echo

exit 0

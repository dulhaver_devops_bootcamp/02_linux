#!/usr/bin/bash

# EXERCISE 9: node app with service user
# extend script 08, so it 
#  - adds a service user 'bootcamp_app01' 
#  - runs the application with this user
#

DOWNLOAD_DIR='https://node-envvars-artifact.s3.eu-west-2.amazonaws.com'
MY_APP='bootcamp-node-envvars-project'
APP_DIR="/opt/ex06/${MY_APP}"
APP_VERSION='1.0.0'
LOG_DIR='/tmp/log/'
APP_USER='bootcamp_appuser01'
# set -x

# check for nodejs/npm and create a return code for each
# njs_rc=$(node --version >/dev/null 2>&1)
# njs_rc=$?
# npm_rc=$(npm --version >/dev/null 2>&1)
# npm_rc=$?
# 
# echo njs_rc: "${njs_rc}" \| npm_rc: "${npm_rc}" ; echo
# sleep 3

# ### install nodejs and npm if they are not installed
# if [ ! ${njs_rc} -eq 0 ] || [ ! ${npm_rc} -eq 0 ] ; then
if ! node --version > /dev/null 2>&1 ; then 	# || npm --version > /dev/null 2>&1 ; then
	echo 'nodejds is not installed, getting it for you now' ; sleep 3 ; echo
	sudo apt update 
	sudo apt install -y ca-certificates curl gnupg
	if [ ! -d /etc/apt/keyrings ] ; then 
		sudo mkdir -p /etc/apt/keyrings
	fi
	curl -fsSL https://deb.nodesource.com/gpgkey/nodesource-repo.gpg.key | \
		sudo gpg --dearmor -o /etc/apt/keyrings/nodesource.gpg
	NODE_MAJOR=20
	echo "deb [signed-by=/etc/apt/keyrings/nodesource.gpg] https://deb.nodesource.com/node_${NODE_MAJOR}.x nodistro main" | \
		sudo tee /etc/apt/sources.list.d/nodesource.list
	sudo apt update && sudo apt install -u nodejs
	# print out which versions were installed
	echo installed node "$(node --version)" \& npm "$(npm --version)" from the nodejs repo ; sleep 3 ; echo
else
	echo ; echo nodejs "$(node --version)" \& npm "$(npm --version)" are already installed ; sleep 3 ; echo
fi

# Download an artifact file from the URL: https://node-envvars-artifact.s3.eu-west-2.amazonaws.com/bootcamp-node-envvars-project-1.0.0.tgz. with curl or wget
if [ ! -d "${APP_DIR}" ] ; then
	sudo mkdir -p "${APP_DIR}" || exit
	# sudo chown "${APP_USER}": "${APP_DIR}"
	cd    "${APP_DIR}" || exit
else
	cd    "${APP_DIR}" || exit
fi

#  - adds a service user 'bootcamp_app01' 
echo  adding a service user "${APP_USER}" now
sudo useradd --home-dir "${APP_DIR}" "${APP_USER}"
# sudo -su $APP_USER

echo I am now in "$(pwd)", proceeding with download as "$(whoami)" in 3 seconds ; sleep 3
sudo curl "${DOWNLOAD_DIR}"/"${MY_APP}"-"${APP_VERSION}".tgz --output "${APP_DIR}"/"${MY_APP}"-"${APP_VERSION}".tgz
# Unzip the downloaded file
sudo tar -xvzf "${MY_APP}"-"${APP_VERSION}".tgz
sudo chown -R "${APP_USER}": "${APP_DIR}"

# Change into the unzipped package directory
cd package || exit

# Set the following needed environment variables: APP_ENV=dev, DB_USER=myuser, DB_PWD=mysecret

runuser -l "${APP_USER}" -c "export APP_ENV=dev"
runuser -l "${APP_USER}" -c "export DB_USER=myuser"
runuser -l "${APP_USER}" -c "export DB_PWD=mysecret"

# get the path for $LOG_DIR from the user & make sure is exists
read -p "gimme logdir (full path): " input
runuser -l "${APP_USER}" -c "export LOG_DIR=${input}"

if [ ! -d "${LOG_DIR}" ] ; then
	mkdir "${LOG_DIR}"
	echo creating "${LOG_DIR}" ; sleep 3
else
	echo "${LOG_DIR}" exists already ; sleep 3
fi

echo these are the set variables: "${APP_ENV}" --- "${DB_USER}" --- "${DB_PWD}" --- "${LOG_DIR}" ; sleep 3


#  - run the application with this user
echo "running the application with this ${APP_USER} user"

echo 

# Run the NodeJS application by executing the following commands:  npm install and node server.js 
runuser -l "${APP_USER}" -c "npm install"
 nohup node server.js & >> "${LOG_DIR}"/node_$(date +%F).log 2>&1
runuser -l "${APP_USER}" -c "node server.js & # >> "${LOG_DIR}"/node_$(date +%F).log 2>&1"
# node server.js & # >> "${LOG_DIR}"/node_$(date +%F).log 2>&1

# check that the application has successfully started
# ps -xU "${USER}" | grep node > /dev/null 2>&1
pgrep -u "${APP_USER}" node


# print out the application's running process and the port where it's listening. 
# export NODE_PROCESS=$(ps -x | grep server.js | head -1 | awk {'print $1'})
NODE_PROCESS=$(sudo pgrep -u "${APP_USER}" node)
export NODE_PROCESS
# export NODE_PORT=$(sudo netstat -tulpn | grep "${NODE_PROCESS}" | awk {'print $4'} | sed "s/://g")
export NODE_PORT=$(ss -tulpn | grep :3000 | awk {'print $5'} | sed "s/*://")

echo ; echo our program runs with process-id: "${NODE_PROCESS}" under port: "${NODE_PORT}"


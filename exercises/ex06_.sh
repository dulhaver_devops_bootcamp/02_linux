# ##############################################################################
#!/usr/bin/bash ################################################################
# ##############################################################################

# EXERCISE 6: Bash Script - Start Node App
# 
# Write a bash script with following logic: 
# 
#   - Install NodeJS and NPM and 
#   - print out which versions were installed
#   - Download an artifact file from the URL: https://node-envvars-artifact.s3.eu-west-2.amazonaws.com/bootcamp-node-envvars-project-1.0.0.tgz. Hint: use curl or wget
#       - Unzip the downloaded file
#       - Set the following needed environment variables: APP_ENV=dev, DB_USER=myuser, DB_PWD=mysecret
#       - Change into the unzipped package directory
#       - Run the NodeJS application by executing the following commands:  npm install and node server.js 
# 
# Notes:
#   - Make sure to run the application in background so that it doesn't block the terminal session where you execute the shell script
#   - If any of the variables is not set, the node app will print error message that env vars is not set and exit
#   - It will give you a warning about LOG_DIR variable not set. You can ignore it for now.
    
# Install NodeJS and NPM

# check whether nodejs and or npm are installed
# nodejs --version >/dev/null 2>&1
# njs_rc=$?

njs_rc=$(nodejs --version >/dev/null 2>&1)
njs_rc=$?
npm_rc=$(npm --version >/dev/null 2>&1)
npm_rc=$?

echo "${njs_rc}" \| "${npm_rc}" ; echo

if [ "{$njs_rc}" != 0 ] ; then
       echo 'nodejds is not installed, getting it for you now'
       sudo curl -fsSL https://deb.nodesource.com/setup_20.x | sudo -E bash - && sudo apt install nodejs -y
       echo 'installed $(nodejs --version) from the nodejs repo'
else
	echo 'nodejs $(nodejs --version) is intalled'
fi





# print out which versions were installed
# Download an artifact file from the URL: https://node-envvars-artifact.s3.eu-west-2.amazonaws.com/bootcamp-node-envvars-project-1.0.0.tgz. Hint: use curl or wget
#   - Unzip the downloaded file
#   - Set the following needed environment variables: APP_ENV=dev, DB_USER=myuser, DB_PWD=mysecret
#   - Change into the unzipped package directory
#   - Run the NodeJS application by executing the following commands:  npm install and node server.js 

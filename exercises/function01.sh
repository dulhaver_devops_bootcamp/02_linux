#!/usr/bin/bash
#
function score_sum {
   sum=0
   while true ; do
      read -p "gimme num: " num
      
      if [ $num == 'q' ] ; then
         echo "seems you got bored with this game. exiting now"
	 break
      fi

      sum=$(( ${sum} + ${num} ))
      echo "u gave me: ${num}, current total is: ${sum}"
   done
}
score_sum

exit 0

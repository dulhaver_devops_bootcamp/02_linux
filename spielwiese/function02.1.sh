################################################################################
#!/usr/bin/bash ################################################################
################################################################################

function touch_file() {

   # make sure trashbin is there
   echo 'creating trashbin'
   if [ ! -d trashbin ] ; then
      mkdir trashbin
   fi

   filename=${*}
   # touching files from execution arguments   
   sleep 1s ; echo "touching files ${filename} in 'trashbin'"
   for file in ${filename} ; do
      touch trashbin/${file[@]}
   done
   
   # list trashbin
   echo trashbin contains $(ls trashbin )
   # cleanup trashbin (remove files older then 2 weeks)
   echo "cleaning up 'trashbin'"
   find trashbin -mtime +7 -delete
}

# calling the 'touch_file' function
touch_file ${*}

exit 0

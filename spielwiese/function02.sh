#!/usr/bin/bash
#

function touch_file() {

   # make sure trashbin is there
   if [ ! -d trashbin ] ; then
      mkdir trashbin
   fi

   # touching files from execution arguments   
   filename=${*}
   echo ${filename} ; sleep 4s
   for i in ${filename} ; do
      touch trashbin/${i[@]}
   done

   # list trashbin
   ls -l trashbin
   # cleanup trashbin (remove files older then 2 weeks)
   find trashbin -mtime +14 -delete
}

# calling the 'touch_file' function
touch_file ${*}

exit 0
